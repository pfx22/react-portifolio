import { HiDownload } from "react-icons/hi";
export function Hero() {
  return (
    <section className="bg-gradient-to-tr from-black to-gray-900 text-white ">
      <div className="container mx-auto max-w-4xl p-2 py-12 flex flex-col md:flex-row">
        <div className="basis-1/2">
          <h1 className="mb-6 text-center md:text-left">
            <span className="font-handwriting block text-3xl ">
              Olá, me chamo{" "}
            </span>
            <span className="font-headline text-5xl font-semibold">Pedro</span>
            <span className="font-headline text-5xl font-semilight text-blue-400">
              {" "}
              Fernandes
            </span>
          </h1>
          <h2 className="font-semibold flex items-center gap-2">
            <div className="h-1 w-12 rounded-full bg-blue-800 text-center md:text-left"></div>
            Desenvolvedor Fullstack - Vue.js | Nest.js{" "}
            <img
              className="invisible sm:visible"
              width="24"
              height="24"
              src="https://www.vectorlogo.zone/logos/vuejs/vuejs-icon.svg"
            />
            <img
            className="invisible sm:visible"
              width="24"
              height="24"
              src="https://www.vectorlogo.zone/logos/nestjs/nestjs-icon.svg"
            />
          </h2>
          <p className="text-gray-400 my-6 text-center md:text-left">
            ⭐ Codificando meu caminho através do mundo binário, um commit de
            cada vez!
          </p>
          <div className="flex items-center justify-center md:justify-start gap-2 ">
            <a
              href="https://wa.me/5511944465655"
              className="font-bold text-white flex"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path
                  fill="#25d366"
                  d="M19.05 4.91A9.816 9.816 0 0 0 12.04 2c-5.46 0-9.91 4.45-9.91 9.91c0 1.75.46 3.45 1.32 4.95L2.05 22l5.25-1.38c1.45.79 3.08 1.21 4.74 1.21c5.46 0 9.91-4.45 9.91-9.91c0-2.65-1.03-5.14-2.9-7.01zm-7.01 15.24c-1.48 0-2.93-.4-4.2-1.15l-.3-.18l-3.12.82l.83-3.04l-.2-.31a8.264 8.264 0 0 1-1.26-4.38c0-4.54 3.7-8.24 8.24-8.24c2.2 0 4.27.86 5.82 2.42a8.183 8.183 0 0 1 2.41 5.83c.02 4.54-3.68 8.23-8.22 8.23zm4.52-6.16c-.25-.12-1.47-.72-1.69-.81c-.23-.08-.39-.12-.56.12c-.17.25-.64.81-.78.97c-.14.17-.29.19-.54.06c-.25-.12-1.05-.39-1.99-1.23c-.74-.66-1.23-1.47-1.38-1.72c-.14-.25-.02-.38.11-.51c.11-.11.25-.29.37-.43s.17-.25.25-.41c.08-.17.04-.31-.02-.43s-.56-1.34-.76-1.84c-.2-.48-.41-.42-.56-.43h-.48c-.17 0-.43.06-.66.31c-.22.25-.86.85-.86 2.07c0 1.22.89 2.4 1.01 2.56c.12.17 1.75 2.67 4.23 3.74c.59.26 1.05.41 1.41.52c.59.19 1.13.16 1.56.1c.48-.07 1.47-.6 1.67-1.18c.21-.58.21-1.07.14-1.18s-.22-.16-.47-.28z"
                />
              </svg>
              &nbsp;Fale Comigo
            </a>
            <span className="italic text-gray-500">ou</span>
            <a
              href="#"
              className="button text-gray-600 hover:text-gray-900 flex items-center gap-2"
              download
            >
              <HiDownload />
              Baixe meu CV
            </a>
          </div>
        </div>
        <div className="basis-1/2"></div>
      </div>
    </section>
  );
}
