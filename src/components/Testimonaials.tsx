export function Testimonials() {
  const tecnologies = [
    {
      name: "HTML 5",
      url: "https://www.svgrepo.com/show/452228/html-5.svg",
    },
    {
      name: "CSS 3",
      url: "https://www.svgrepo.com/show/452185/css-3.svg",
    },
    {
      name: "Javascript ES6+",
      url: "https://www.svgrepo.com/show/452045/js.svg",
    },
    {
      name: "Typescript",
      url: "https://www.svgrepo.com/show/354478/typescript-icon.svg",
    },
    {
      name: "Bootstrap",
      url: "https://www.svgrepo.com/show/353498/bootstrap.svg",
    },
    {
      name: "Tailwind",
      url: "https://www.svgrepo.com/show/374118/tailwind.svg",
    },
    {
      name: "Vue.js",
      url: "https://www.svgrepo.com/show/354528/vue.svg",
    },
    {
      name: "Nuxt.js",
      url: "https://www.svgrepo.com/show/373940/nuxt.svg",
    },
    {
      name: "Quasar",
      url: "https://www.svgrepo.com/show/374024/quasar.svg",
    },
    {
      name: "React.js",
      url: "https://www.svgrepo.com/show/349488/react.svg",
    },
    {
      name: "Node.js",
      url: "https://www.svgrepo.com/show/354119/nodejs-icon.svg",
    },
    {
      name: "Nest.js",
      url: "https://www.svgrepo.com/show/354107/nestjs.svg",
    },
    {
      name: "Jest",
      url: "https://www.svgrepo.com/show/353930/jest.svg",
    },
    {
      name: "Python",
      url: "https://www.svgrepo.com/show/452091/python.svg",
    },
    {
      name: "FastAPI",
      url: "https://www.svgrepo.com/show/330413/fastapi.svg",
    },
    {
      name: "Keycloak Auth",
      url: "https://www.svgrepo.com/show/331455/keycloak.svg",
    },
    {
      name: "JWT",
      url: "https://www.svgrepo.com/show/306280/jsonwebtokens.svg",
    },
    {
      name: "MySQL",
      url: "https://www.svgrepo.com/show/373848/mysql.svg",
    },
    {
      name: "MongoDB",
      url: "https://www.svgrepo.com/show/331488/mongodb.svg",
    },
    {
      name: "AWS",
      url: "https://www.svgrepo.com/show/394021/aws.svg",
    },
    {
      name: "Digital Ocean",
      url: "https://www.svgrepo.com/show/448218/digital-ocean.svg",
    },
    {
      name: "Docker",
      url: "https://www.svgrepo.com/show/353659/docker-icon.svg",
    },
    {
      name: "Vite",
      url: "https://www.svgrepo.com/show/354521/vitejs.svg",
    },
    {
      name: "Git",
      url: "https://www.svgrepo.com/show/303548/git-icon-logo.svg",
    },
  ];
  return (
    <section className="container mx-auto max-w-4xl p-4 py-8">
      <div className="row">
        <div className=" grid grid-cols-4 md:grid-cols-6 gap-2 content-center bg-">
          {tecnologies.map((tecnologies, idx) => (
            <div
              key={`tecnologies-${idx}`}
              className="flex flex-col justify-center items-center"
            >
              <img src={tecnologies.url} width={30} />
              <p className="text-xs font-headline my-1">{tecnologies.name}</p>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
