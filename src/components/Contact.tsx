import { FaWhatsapp } from "react-icons/fa";
import {
  HiOutlineEnvelope,
  HiOutlineMapPin,
} from "react-icons/hi2";

export function Contact() {
  const contacts = [
    {
      name: "WhatsApp",
      description: "+55 11 9 4446-5655",
      link: "https://wa.me/5511944465655",
      icon: <FaWhatsapp className="h-10 w-10" />,
    },
    {
      name: "Email",
      description: "pfxfernandes14@gmail.com",
      link: "mailto:pfxfernandes14@gmail.com?subject=Olá...",
      icon: <HiOutlineEnvelope className="h-10 w-10" />,
    },
    {
      name: "São Paulo",
      description: "",
      link: "https://www.google.com.br/maps/place/S%C3%A3o+Paulo,+SP/@-23.6820636,-46.9249429,10z/data=!3m1!4b1!4m6!3m5!1s0x94ce448183a461d1:0x9ba94b08ff335bae!8m2!3d-23.5557714!4d-46.6395571!16zL20vMDIycGZt?entry=ttu",
      icon: <HiOutlineMapPin className="h-10 w-10" />,
    },
  ];

  return (
    <section id="contact" className="bg-blue-700 text-white">
      <div className="container mx-auto max-w-4xl p-4 py-8">
        <div className="mb-6 text-center">
          <h2 className="z-50 mb-2">
            <span className="mr-2 font-headline text-3xl font-semibold">
              Fale
            </span>
            <span className="font-handwriting text-4xl">Comigo</span>
          </h2>
          <p className="text-sm">
            Entre em contato por formulário ou WhatsApp, com certeza irei poder
            te ajudar.
          </p>
        </div>
        <div className=" gap-6 md:flex-row">
          <div className="flex flex-col ">
            {contacts.map((contact, index) => (
              <div
                key={`contact-${index}`}
                className="basis-1/3  mb-4 flex items-center gap-4 rounded-lg border border-dashed border-gray-400 p-4"
              >
                {contact.icon}
                <div>
                  <p className="font-headline font-semibold">{contact.name}</p>
                  <a
                    href={contact.link}
                    target="_blank"
                    className="text-gray-300 underline underline-offset-2"
                  >
                    {contact.description}
                  </a>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}
